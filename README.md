# Algorithms
Various algorithms in various languages

## Setup

Make sure you have conan and cmake installed

```
> mkdir build && cd build
> conan install ..
> conan build ..
> make
```

## Tests
To run tests:
```
> make test
```

To run individual test
```
> bin/*test.exe
```