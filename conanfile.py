from conans import ConanFile
from conans import CMake

class ConanAlgorithms(ConanFile):
    name = "algorithms"
    version = "0.1.0"
    settings = "os", "arch", "compiler", "build_type"
    generators = "cmake"
    description = "Nice"
    requires = "gtest/1.8.1@bincrafters/stable"
    default_options = "gtest:shared=True"

    def build(self):
        self.cmake = CMake(self)
        self.cmake.configure()
        self.cmake.build()

    def imports(self):
        self.copy("*.so", "bin", "lib")
        self.copy("*.dll", "bin", "bin")
        self.copy("*.dylib", "bin", "lib")

    def test(self):
        target_test = "RUN_TESTS" if self.settings.os == "Windows" else "test"
        self.cmake.build(target=target_test)
