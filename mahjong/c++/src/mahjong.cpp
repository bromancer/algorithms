#include "mahjong.h"
#include <sstream>

namespace Mahjong {
    // Tile
    Tile::Tile(TileType tile_type) : tile_type(tile_type) {}
    bool Tile::isSameSort(Tile &other) {
        if (
            this->tile_type >= TileType::MAN1 &&
            this->tile_type <= TileType::MAN9 &&
            other.tile_type >= TileType::MAN1 &&
            other.tile_type <= TileType::MAN9) {
            return true;
        }

        if (
            this->tile_type >= TileType::PIN1 &&
            this->tile_type <= TileType::PIN9 &&
            other.tile_type >= TileType::PIN1 &&
            other.tile_type <= TileType::PIN9) {
            return true;
        }

        if (
            this->tile_type >= TileType::SOU1 &&
            this->tile_type <= TileType::SOU9 &&
            other.tile_type >= TileType::SOU1 &&
            other.tile_type <= TileType::SOU9) {
            return true;
        }

        return this->tile_type == other.tile_type;
    }
    bool Tile::isSuitTile() {
        return this->tile_type >= TileType::MAN1 && this->tile_type <= TileType::SOU9;
    }
    int Tile::getNumberIndex() {
        if (this->tile_type >= TileType::MAN1 && this->tile_type <= TileType::MAN9) {
            return this->tile_type - TileType::MAN1;
        }

        if (this->tile_type >= TileType::PIN1 && this->tile_type <= TileType::PIN9) {
            return this->tile_type - TileType::PIN1;
        }

        if (this->tile_type >= TileType::SOU1 && this->tile_type <= TileType::SOU9) {
            return this->tile_type - TileType::SOU1;
        }
    }
    std::string Tile::toString() {
        switch (this->tile_type) {
        case BLANK: return "X";
        case MAN1: return "M1";
        case MAN2: return "M2";
        case MAN3: return "M3";
        case MAN4: return "M4";
        case MAN5: return "M5";
        case MAN6: return "M6";
        case MAN7: return "M7";
        case MAN8: return "M8";
        case MAN9: return "M9";
        case PIN1: return "P1";
        case PIN2: return "P2";
        case PIN3: return "P3";
        case PIN4: return "P4";
        case PIN5: return "P5";
        case PIN6: return "P6";
        case PIN7: return "P7";
        case PIN8: return "P8";
        case PIN9: return "P9";
        case SOU1: return "S1";
        case SOU2: return "S2";
        case SOU3: return "S3";
        case SOU4: return "S4";
        case SOU5: return "S5";
        case SOU6: return "S6";
        case SOU7: return "S7";
        case SOU8: return "S8";
        case SOU9: return "S9";
        case TON: return "EW";
        case WNAN: return "SW";
        case SHAA: return "WW";
        case PEI: return "NW";
        case HAKU: return "WD";
        case HATSU: return "GD";
        case CHUN: return "RD";
        default: return "";
        }
    }

    // TileMeld
    TileMeld::TileMeld(TileP tile1, TileP tile2, TileP tile3, bool is_closed) :
        tile1(tile1),
        tile2(tile2),
        tile3(tile3),
        tile4(nullptr),
        is_closed(is_closed),
        is_kan(false)
    {}
    TileMeld::TileMeld(TileP tile1, TileP tile2, TileP tile3, TileP tile4, bool is_closed) :
        tile1(tile1),
        tile2(tile2),
        tile3(tile3),
        tile4(tile4),
        is_closed(is_closed),
        is_kan(true)
    {}
    bool TileMeld::isClosed() { return this->is_closed; }
    bool TileMeld::isKan() { return this->is_kan; }
    std::string TileMeld::toString() {
        std::stringstream ss;
        ss << "(" << tile1->toString() << "," << tile2->toString() << "," << tile3->toString();
        if (tile4 != nullptr) {
            ss << "," + tile4->toString();
        }

        ss << "[" << (this->is_closed ? "C" : "O") << "]" << ")";

        return ss.str();
    }

    // TilePair
    TilePair::TilePair(TileP tile1, TileP tile2) : tile1(tile1), tile2(tile2) {}
    std::string TilePair::toString() {
        return "(" + tile1->toString() + "," + tile2->toString() + ")";
    }

    // HandReading
    HandReading::HandReading() : is_kokushi(false), valid_keishiki(false) {}
    HandReading::HandReading(std::vector<TileMeldP> melds, TilePairP pair) : melds(melds), is_kokushi(false) {
        this->pairs.push_back(pair);

        for (auto meld : melds) {
            this->tiles.push_back(meld->tile1);
            this->tiles.push_back(meld->tile2);
            this->tiles.push_back(meld->tile3);

            if (meld->isKan()) {
                this->tiles.push_back(meld->tile4);
            }
        }

        this->tiles.push_back(pair->tile1);
        this->tiles.push_back(pair->tile2);

        this->valid_keishiki = HandReading::checkKeishiki(this->tiles);
    }
    void HandReading::addMeld(TileMeldP meld) {
        this->melds.push_back(meld);

        this->tiles.push_back(meld->tile1);
        this->tiles.push_back(meld->tile2);
        this->tiles.push_back(meld->tile3);

        if (meld->isKan()) {
            this->tiles.push_back(meld->tile4);
        }
    }
    bool HandReading::checkKeishiki(std::vector<TileP> &tiles) {
        for (auto tile : tiles) {
            int count = 0;

            for (auto t : tiles) {
                if (tile->tile_type == t->tile_type) {
                    count++;

                    if (count > 4) {
                        return false;
                    }
                }
            }
        }

        return true;
    }
    std::string HandReading::toString() {
        std::stringstream ss;

        ss << "tiles: ";

        for (auto tile : tiles) {
            ss << tile->toString() << " ";
        }

        ss << std::endl;

        ss << "melds: ";

        for (auto meld : melds) {
            ss << meld->toString() << " ";
        }

        ss << std::endl;

        ss << "pairs: ";

        for (auto pair : pairs) {
            ss << pair->toString() << " ";
        }

        ss << std::endl;

        return ss.str();
    }

    // Logics
    std::vector<HandReadingP> hand_reading_recursion(std::vector<TileP> &remaining_tiles, std::vector<TileMeldP> &melds, bool tenpai_only, bool early_return) {
        std::vector<HandReadingP> readings;
        auto hand = remaining_tiles;

        // Assume tiles are sorted

        // @TODO: Perform sanity check (correct number of tiles)
        if (hand.size() % 3 != 2) {
            return readings;
        }

        if (hand.size() == 1) {
            // @TODO: Tenpai rule
            return readings;
        }

        if (hand.size() == 2) {
            if (hand[0]->tile_type == hand[1]->tile_type) {
                auto pair = std::make_shared<TilePair>(hand[0], hand[1]);

                auto reading = std::make_shared<HandReading>(melds, pair);

                if (reading->valid_keishiki) {
                    readings.push_back(reading);
                }
            }

            return readings;
        }


        if (hand.size() == 13 || hand.size() == 14) {
            // @TODO: 7 Pairs and 13 Orphans
            if (false) {
                return readings;
            }
        }

        for (int i = 0; i < hand.size(); i++) {
            if (i != 0 && hand[i]->tile_type == hand[i - 1]->tile_type) {
                continue;
            }

            auto tile = hand[i];

            std::vector<TileP> copy;
            std::vector<TileP> meld;

            for (auto t : hand)
            {
                if (meld.size() < 3 && tile->tile_type == t->tile_type) {
                    meld.push_back(t);
                }
                else {
                    copy.push_back(t);
                }
            }

            if (meld.size() == 3)
            {
                auto m = std::make_shared<TileMeld>(meld[0], meld[1], meld[2], true);

                // Abuse copy constructor to clone
                std::vector<TileMeldP> new_melds = melds;
                new_melds.push_back(m);

                auto r = hand_reading_recursion(copy, new_melds, tenpai_only, early_return);
                readings.insert(readings.end(), r.begin(), r.end());

                // @TODO: Early return?
            }

            if (tile->isSuitTile() && tile->getNumberIndex() <= 6)
            {
                TileP one_more = nullptr;
                TileP two_more = nullptr;

                copy.clear();

                for (auto t : hand) {
                    if (t == tile) {
                        continue;
                    }

                    if (t->tile_type - tile->tile_type == 1 && one_more == nullptr) {
                        one_more = t;
                    }
                    else if (t->tile_type - tile->tile_type == 2 && two_more == nullptr) {
                        two_more = t;
                    }
                    else {
                        copy.push_back(t);
                    }
                }

                if (one_more != nullptr && two_more != nullptr) {
                    auto m = std::make_shared<TileMeld>(tile, one_more, two_more, true);

                    // Abuse copy constructor to clone
                    std::vector<TileMeldP> new_melds = melds;
                    new_melds.push_back(m);

                    auto r = hand_reading_recursion(copy, new_melds, tenpai_only, early_return);

                    readings.insert(readings.end(), r.begin(), r.end());


                    // @TODO: Early return?
                }
            }

            if (hand.size() == 4) {
                int s = hand.size();

                TileP t = nullptr;
                TileP n1 = nullptr;
                TileP n2 = nullptr;

                // @TODO: Tenpai only path
            }
        }

        return readings;
    }
}
