#pragma once

#include <vector>
#include <memory>

namespace Mahjong {
    enum TileType {
        BLANK = 0,
        MAN1,  // Character 1
        MAN2,
        MAN3,
        MAN4,
        MAN5,
        MAN6,
        MAN7,
        MAN8,
        MAN9,
        PIN1,  // Pin 1
        PIN2,
        PIN3,
        PIN4,
        PIN5,
        PIN6,
        PIN7,
        PIN8,
        PIN9,
        SOU1,  // Bamboo 1
        SOU2,
        SOU3,
        SOU4,
        SOU5,
        SOU6,
        SOU7,
        SOU8,
        SOU9,
        TON,   // East
        WNAN,  // South
        SHAA,  // West
        PEI,   // North
        HAKU,  // White Dragon
        HATSU, // Green Dragon
        CHUN   // Red Dragon
    };

    class Tile {
    public:
        TileType tile_type;
        Tile(TileType tile_type);
        bool isSameSort(Tile &other);
        bool isSuitTile();
        //bool isHonorTile();
        //bool isDragonTile();
        int getNumberIndex();
        std::string toString();
    };
    typedef std::shared_ptr<Tile> TileP;

    class TileMeld {
    public:
        TileP tile1;
        TileP tile2;
        TileP tile3;
        TileP tile4;
        bool is_closed;
        bool is_kan;
        TileMeld(TileP tile1, TileP tile2, TileP tile3, bool is_closed);
        TileMeld(TileP tile1, TileP tile2, TileP tile3, TileP tile4, bool is_closed);
        bool isKan();
        bool isClosed();
        std::string toString();
    };
    typedef std::shared_ptr<TileMeld> TileMeldP;

    class TilePair {
    public:
        TileP tile1;
        TileP tile2;
        TilePair(TileP tile1, TileP tile2);
        std::string toString();
    };
    typedef std::shared_ptr<TilePair> TilePairP;

    class HandReading {
    public:
        bool valid_keishiki;
        bool is_kokushi;
        std::vector<TileMeldP> melds;
        std::vector<TilePairP> pairs;
        std::vector<TileP> tiles;
        HandReading(); // Empty
        HandReading(std::vector<TileMeldP> melds, TilePairP pair); // Normal
        //HandReading(std::vector<TilePair*> pairs); // 7 Pairs
        //HandReading(std::vector<Tile*> tiles); // 13 orphans
        void addMeld(TileMeldP meld);
        static bool checkKeishiki(std::vector<TileP> &tiles);
        std::string toString();
    };
    typedef std::shared_ptr<HandReading> HandReadingP;

    extern std::vector<HandReadingP> hand_reading_recursion(std::vector<TileP> &remaining_tiles, std::vector<TileMeldP> &melds, bool tenpai_only, bool early_return);

}
