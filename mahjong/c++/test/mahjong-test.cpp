#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <stdio.h>

#include "mahjong.h"

using namespace std;
using namespace Mahjong;

TEST(MahjongTest, HandReadingRecursion)
{
    std::vector<std::shared_ptr<Tile>> tiles;

    tiles.push_back(std::make_shared<Tile>(TileType::MAN1));
    tiles.push_back(std::make_shared<Tile>(TileType::MAN2));
    tiles.push_back(std::make_shared<Tile>(TileType::MAN3));

    tiles.push_back(std::make_shared<Tile>(TileType::PIN1));
    tiles.push_back(std::make_shared<Tile>(TileType::PIN2));
    tiles.push_back(std::make_shared<Tile>(TileType::PIN3));

    tiles.push_back(std::make_shared<Tile>(TileType::SOU1));
    tiles.push_back(std::make_shared<Tile>(TileType::SOU2));
    tiles.push_back(std::make_shared<Tile>(TileType::SOU3));

    tiles.push_back(std::make_shared<Tile>(TileType::HAKU));
    tiles.push_back(std::make_shared<Tile>(TileType::HAKU));
    tiles.push_back(std::make_shared<Tile>(TileType::HAKU));

    tiles.push_back(std::make_shared<Tile>(TileType::TON));
    tiles.push_back(std::make_shared<Tile>(TileType::TON));

    std::vector<TileMeldP>melds;
    std::vector<HandReadingP> readings = hand_reading_recursion(tiles, melds, false, false);

    std::cerr << "Number of readings: " << readings.size() << std::endl;

    for (auto r : readings) {
        std::cerr << r->toString() << std::endl;
    }
}