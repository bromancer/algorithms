#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <stdlib.h>

using namespace ::testing;

/**
 * Fills an array with random numbers between 0 and 1000
 */
void
fill_random_array(int *array, unsigned int size) {
    for(unsigned int i = 0; i < size; i++) {
        array[i] = rand() % 1000;
    }
}

/**
 * Checks if an array's elements are all larger or equal to their
 * predecessor
 */
void
walk_array(int *array, unsigned int size) 
{
    if (size < 2) 
    {
        return;
    }

    for (unsigned int index = 1; index < size; index++)
    {
        ASSERT_THAT(array[index - 1], Le(array[index]));
    }
}
