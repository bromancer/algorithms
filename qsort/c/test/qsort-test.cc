#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "lomuto_qsort.h"
#include "hoare_qsort.h"

#include "array-walker.cc"

using namespace ::testing;

TEST(LomutoQuickSortTest, SortArrayOfLength2)
{
    int array[] = { 2, 1 };

    lomuto(array, 2);

    ASSERT_THAT(array, ElementsAreArray({ 1, 2} ));
}

TEST(LomutoQuickSortTest, SortArrayOfLength3)
{
    int array[] = { 2, 1, 9 };

    lomuto(array, 3);

    ASSERT_THAT(array, ElementsAreArray({ 1, 2, 9 }));
}

TEST(LomutoQuickSortTest, SortArrayOfLength4)
{
    int array[] = { 2, 1, 9, 4 };

    lomuto(array, 4);

    ASSERT_THAT(array, ElementsAreArray({ 1, 2, 4, 9 }));
}

TEST(LomutoQuickSortTest, SortAlreadySortedArray)
{
    int array[6] = { 1, 2, 3, 4, 5, 6 };

    lomuto(array, 6);

    ASSERT_THAT(array, ElementsAreArray({ 1, 2, 3, 4, 5, 6}));
}

TEST(LomutoQuickSortTest, RandomArray)
{
    int array[100];

    fill_random_array(array, 100);

    lomuto(array, 100);

    walk_array(array, 100);
}

TEST(HoareQuickSortTest, SortArrayOfLength2)
{
    int array[] = { 2, 1 };

    hoare(array, 2);

    ASSERT_THAT(array, ::testing::ElementsAreArray({ 1, 2} ));
}

TEST(HoareQuickSortTest, SortArrayOfLength3)
{
    int array[] = { 2, 1, 9 };

    hoare(array, 3);

    ASSERT_THAT(array, ::testing::ElementsAreArray({ 1, 2, 9 }));
}

TEST(HoareQuickSortTest, SortArrayOfLength4)
{
    int array[] = { 2, 1, 9, 4 };

    hoare(array, 4);

    ASSERT_THAT(array, ::testing::ElementsAreArray({ 1, 2, 4, 9 }));
}

TEST(HoareQuickSortTest, SortAlreadySortedArray)
{
    int array[6] = { 1, 2, 3, 4, 5, 6 };

    hoare(array, 6);

    ASSERT_THAT(array, ::testing::ElementsAreArray({ 1, 2, 3, 4, 5, 6}));
}

TEST(HoareQuickSortTest, RandomArray)
{
    int array[100] = {};

    fill_random_array(array, 100);

    hoare(array, 100);

    walk_array(array, 100);
}

int
main(int argc, char** argv)
{
    InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
