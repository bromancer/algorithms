#include "hoare_qsort.h"

static void
swap(int *left, int *right)
{
    int temp = *left;
    *left = *right;
    *right = temp;
}

int
hoare_partition(int *array, int low, int high)
{
    int pivot = array[low];
    int i = low - 1;
    int j = high + 1;

    while(1)
    {
        do
        {
            i++;
        } while(array[i] < pivot);

        do
        {
            j--;
        } while(array[j] > pivot);

        if (i >= j)
        {
            return j;
        }

        swap(&array[i], &array[j]);
    }
}

void
hoare_qsort(int *array, int low, int high)
{
    if (low >= high)
    {
        return;
    }

    int p = hoare_partition(array, low, high);
    hoare_qsort(array, low, p);
    hoare_qsort(array, p + 1, high);
}

void
hoare(int *array, int length)
{
    hoare_qsort(array, 0, length - 1);
}
