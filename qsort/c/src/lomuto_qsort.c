#include "lomuto_qsort.h"

static void
swap(int *left, int *right)
{
    int temp = *left;
    *left = *right;
    *right = temp;
}

int
lomuto_partition(int *array, int low, int high)
{
    int pivot = array[high];
    int i = low - 1;

    for (int j = low; j <= high - 1; j++)
    {
        if (array[j] <= pivot)
        {
            i++;
            swap(&array[i], &array[j]);
        }
    }

    swap(&array[++i], &array[high]);
    return i;
}

void
lomuto_qsort(int *array, int low, int high)
{
    if (low >= high)
    {
        return;
    }

    int p = lomuto_partition(array, low, high);
    lomuto_qsort(array, low, p - 1);
    lomuto_qsort(array, p + 1, high);
}

void
lomuto(int *array, int length)
{
    lomuto_qsort(array, 0, length - 1);
}
